#include <Arduino.h>
#include "Gsender.h"
#include <ESP8266WiFi.h>

#define LED 2
#define BOTON 0

String correos[] = {"aldairparrag@gmail.com", "ramonparra@supramax.com.mx", "ramon.parra@chipred.com"};

//Bandera para enviar correo
bool sentEmailFlg = false;

void connectToWifi();
void sendEmail(String asunto);

void setup()
{
  pinMode(LED, OUTPUT);
  pinMode(BOTON, INPUT);
  digitalWrite(LED, HIGH);

  Serial.begin(115200);
  Serial.println("Inicio de programa...");

  connectToWifi();
}

void loop()
{
  if (digitalRead(BOTON) == LOW)
  {
    if (!sentEmailFlg)
      sendEmail("Pruebas GmailSender");

    while (digitalRead(BOTON) == LOW)
    {
      delay(10);
    }
  }

  delay(200);
}

void connectToWifi()
{
  Serial.println("Conectando a la red...");
  WiFi.begin("AP_OFICINA", "B1n4r1uM");

  while (WiFi.status() != WL_CONNECTED)
  {
    digitalWrite(LED, !digitalRead(LED));
    delay(150);
  }

  Serial.println("Conectado :D");
}

void sendEmail(String asunto)
{
  Serial.println("Enviando correo electronico...");

  Gsender *gsender = Gsender::Instance();
  if (gsender->Subject(asunto)->Send(3, correos, "Saludos desde la oficina de los de Sistemas"))
  {
    Serial.println("Se envió el correo");
  }
  else
  {
    Serial.println("Error al enviar el correo");
    Serial.println(gsender->getError());
  }

  sentEmailFlg = true;
}
